## How to run

> git clone git@gitlab.com:marin-sergey/questionnaire.git
> 
> cd questionnaire

Prepare .env file. Or just copy-paste this sample:
Make sure that exposed ports for mysql & web-server are not in use.

```text
PHP_WITH_XDEBUG=false
DOCKER_HOST_IP=172.17.0.1

# run `id -u` and place result value
DID=1000
# run `id -g` and place result value
GID=1000

# port to be exposed to access through localhost
PHP_DEV_SERVER_PORT=8181

# mysql related vars
MYSQL_DATABASE=qa
MYSQL_USER=qa
MYSQL_PASSWORD=qapass
MYSQL_ROOT_PASSWORD=rootpass
# port to listen on host
MYSQL_EXPOSE_PORT=3466
MYSQL_HOST=mysql
MYSQL_PORT=3306

APP_NAME=questionnaire
APP_ENV=local
APP_KEY=base64:5kbFGmUutC/3mPZbSkPDmTYk7bhti0PpoEm2UVKZ11s=
APP_DEBUG=true
```

> docker-compose run php composer install
> 
> docker-compose up
 
The last command will start php dev-server and mysql database. 
To prepare DB, open new console and execute following commands:

> docker-compose exec php ./artisan doctrine:migrations:migrate
> 
> docker-compose exec php ./artisan db:seed

To run test:

> docker-compose exec php bin/phpunit

### To populate DB with 100k answers:

There are two ways:

1. Using fake unit-test. Open file `tests/Feature/ApiPopulationTest.php` and comment line started with `$this->markTestSkipped(` and run:

> docker-compose exec php bin/phpunit tests/Feature/ApiPopulationTest.php

2. Using command:

> docker-compose exec php ./artisan artisan 10000

This process can be executed in parallel, by setting start position:

> docker-compose exec php ./artisan app:questionnaire:populate 5000 0
> 
> docker-compose exec php ./artisan app:questionnaire:populate 5000 5000

## The task

Create API with necessary endpoints to populate answers for questions

##### Input

JSON array containing list of question ID, user token (ID, but not secure), and answer to each Question.

Count of questions of single type must be limited.

In current task, it MUST contain 9 `numerical` and 1 `open` questions.

Answers in `numerical` type can be scalar from 0 to 5 

##### Output

There are two parts. 

For `numerical` type: 

- average numerical answer value in that questions
- question answers count
- answers per question option
  
For `open` type:
- question answers count
- answers per question option

## The Model

#### Object `Questionnaire`

This object stores denormalized input.
Each `Questionnaire` contains two collections: Questions and Answers.

#### Questions

This is dictionary, containing ID, question text and type (`numerical`, `open`)

#### Answers

There are two type of `Answer` objects. `AnswerNumerical` and `AnswerOpen`

Relation type between `Questions` and `Answer` - One-To-Many. 
Each `Question` can relate to many `Answer` objects. 
Each `Answer` relates to one `Question`.

#### Limitations

Each `Question` object have limitation for number of related `AnswerNumerical` and `AnswerOpen`.
This rule should be set as parameter for domain service.


## Storage Denormalization

According task description, some aggregated values should be returned after each API request. 
To prevent resource-intensive aggregating operations on database, values should be stored in Question object.

This fields may be recalculated by special command, that have to be implemented. 





