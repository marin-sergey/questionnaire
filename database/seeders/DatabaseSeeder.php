<?php

namespace Database\Seeders;

use App\Entities\Questionnaire\Question;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->entityManager->createQuery('delete from '.Question::class)->execute();

        for ($i=0;$i<9;$i++) {
            $question = (new Question())
                ->setType(Question::TYPE_NAME_NUMERICAL)
                ->setQuestionText("Question #${i}");

            $this->entityManager->persist($question);
        }

        $question = (new Question())
            ->setType(Question::TYPE_NAME_OPEN)
            ->setQuestionText("Open Question #1");

        $this->entityManager->persist($question);

        $this->entityManager->flush();
    }
}
