<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20210928215730 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questionnaire__answer_numerical (id INT AUTO_INCREMENT NOT NULL, question_id INT DEFAULT NULL, answer SMALLINT UNSIGNED NOT NULL, user_id INT NOT NULL, INDEX IDX_26CBBA01E27F6BF (question_id), INDEX an_user_question_idx (user_id, question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questionnaire__answer_open (id INT AUTO_INCREMENT NOT NULL, question_id INT DEFAULT NULL, answer VARCHAR(255) NOT NULL, user_id INT NOT NULL, INDEX IDX_9F01D6881E27F6BF (question_id), INDEX ao_user_question_idx (user_id, question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questionnaire__question (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, question_text VARCHAR(1024) NOT NULL, total INT NOT NULL, sum INT NOT NULL, average NUMERIC(6, 2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE questionnaire__answer_numerical ADD CONSTRAINT FK_26CBBA01E27F6BF FOREIGN KEY (question_id) REFERENCES questionnaire__question (id)');
        $this->addSql('ALTER TABLE questionnaire__answer_open ADD CONSTRAINT FK_9F01D6881E27F6BF FOREIGN KEY (question_id) REFERENCES questionnaire__question (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE questionnaire__answer_numerical DROP FOREIGN KEY FK_26CBBA01E27F6BF');
        $this->addSql('ALTER TABLE questionnaire__answer_open DROP FOREIGN KEY FK_9F01D6881E27F6BF');
        $this->addSql('DROP TABLE questionnaire__answer_numerical');
        $this->addSql('DROP TABLE questionnaire__answer_open');
        $this->addSql('DROP TABLE questionnaire__question');
    }
}
