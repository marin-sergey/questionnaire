<?php

declare(strict_types=1);

namespace Tests\Questionnaire;

use App\Entities\Questionnaire\Question;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;

trait MockEntityHelperTrait
{
    public function getQuestion(int $questionId):Question
    {
        $mock = $this->createMock(Question::class);
        $mock->expects($this->any())->method('getId')->willReturn($questionId);
        return $mock;
    }
}
