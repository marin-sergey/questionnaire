<?php

declare(strict_types=1);

namespace Tests\Questionnaire;

use App\Entities\Questionnaire\AnswerInterface;
use App\Entities\Questionnaire\AnswerNumerical;
use App\Entities\Questionnaire\AnswerOpen;
use App\Entities\Questionnaire\Question;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class AnswerObjectTest extends TestCase
{
    use MockEntityHelperTrait;

    /**
     * @dataProvider provideQuestions
     */
    public function testAnswerInstantiation(string $answerType, int $questionId, string|int $answer, ?string $exception = null): void
    {
        if ($exception) {
            $this->expectException($exception);
        }

        /** @var AnswerInterface $answer */
        $answerObj = new $answerType();
        $answerObj->setQuestion($this->getQuestion($questionId))
            ->setAnswer($answer);

        $this->assertInstanceOf($answerType, $answerObj);
    }

    /**
     * @dataProvider provideQuestions
     */
    public function testAggregation(string $answerType, int $questionId, string|int $answer, ?string $exception = null): void
    {
        if ($exception) {
            $this->expectException($exception);
        }

        $question = (new Question())
            ->setType($answerType === AnswerOpen::class ? Question::TYPE_NAME_OPEN : Question::TYPE_NAME_NUMERICAL)
            ->setQuestionText('Some text');

        /** @var AnswerInterface $answer */
        $answerObj = new $answerType();
        $answerObj->setQuestion($this->getQuestion($questionId))
            ->setAnswer($answer);

        $question->updateAggregatedValues($answerObj);

        $this->assertEquals(1, $question->getTotal());
        $this->assertEquals(1, $question->getSum());
        $this->assertEquals(1, $question->getAverage());
    }

    public function provideQuestions(): \Generator
    {
        yield [AnswerNumerical::class, 1, 1];
        yield [AnswerOpen::class, 2, 'Word'];
        yield [AnswerOpen::class, 3, 1, \TypeError::class]; // invalid type
        yield [AnswerNumerical::class, 4, 'The Text Answer on numerical', \TypeError::class]; // invalid type
    }
}
