<?php

declare(strict_types=1);

namespace Tests\Questionnaire;

use App\Entities\Questionnaire\AnswerNumerical;
use App\Entities\Questionnaire\AnswerOpen;
use App\Request\QuestionnaireRequest;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class QuestionnaireRequestTest extends TestCase
{
    use MockEntityHelperTrait;
    /**
     * @dataProvider provideQuestionnaire
     */
    public function testQuestionnaireRequestInstantiation(array $answers, int $expectedNumericalCount, $expectedOpenCount): void
    {
        $questionnaire = new QuestionnaireRequest();

        foreach ($answers as $answer) {
            $questionnaire->addAnswer($answer);
        }

        $this->assertInstanceOf(QuestionnaireRequest::class, $questionnaire);
        $this->assertCount($expectedNumericalCount, $questionnaire->getNumericalAnswers());
        $this->assertCount($expectedOpenCount, $questionnaire->getOpenAnswers());
    }

    public function provideQuestionnaire(): \Generator
    {
        yield [
                [
                    (new AnswerNumerical())->setQuestion($this->getQuestion(1))->setAnswer(1)->setUserId(1),
                    (new AnswerNumerical())->setQuestion($this->getQuestion(2))->setAnswer(1)->setUserId(1),
                    (new AnswerOpen())->setQuestion($this->getQuestion(3))->setAnswer('Text answer')->setUserId(1),
                ], 2, 1,
            ];
        yield [
                [
                    (new AnswerOpen())->setQuestion($this->getQuestion(1))->setAnswer('Test open 1')->setUserId(2),
                    (new AnswerNumerical())->setQuestion($this->getQuestion(3))->setAnswer(1)->setUserId(2),
                    (new AnswerOpen())->setQuestion($this->getQuestion(2))->setAnswer('Test open 2')->setUserId(2),
                ], 1, 2,
            ];
    }
}
