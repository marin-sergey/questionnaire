<?php

declare(strict_types=1);

namespace Tests\Questionnaire;

use App\Entities\Questionnaire\AnswerInterface;
use App\Entities\Questionnaire\AnswerNumerical;
use App\Entities\Questionnaire\AnswerOpen;
use App\Entities\Questionnaire\Question;
use App\Exception\InvalidRequestException;
use App\Request\QuestionnaireRequest;
use App\Service\Database\DatabaseServiceInterface;
use App\Service\Domain\Questionnaire\QuestionAnswerService;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class QuestionnaireServiceTest extends TestCase
{
    use MockEntityHelperTrait;

    public function testQuestionnaireServiceInstantiation(): void
    {
        $databaseService = $this->createMock(DatabaseServiceInterface::class);
        $questionnaireService = $this->getQuestionAnswerService($databaseService, 1, 1);
        $this->assertInstanceOf(QuestionAnswerService::class, $questionnaireService);
    }

    /**
     * @dataProvider provideQuestions
     */
    public function testQuestionnaireServiceIsValid(QuestionnaireRequest $questionnaireRequest, int $expectOpen, int $expectNumerical, bool $expectedIsValid): void
    {
        $databaseService = $this->createMock(DatabaseServiceInterface::class);
        $questionnaireService = $this->getQuestionAnswerService($databaseService, $expectOpen, $expectNumerical);

        if (!$expectedIsValid) {
            $this->expectException(InvalidRequestException::class);
        }

        $isValid = $questionnaireService->isValid($questionnaireRequest);

        $this->assertTrue($isValid);
    }

    /**
     * @dataProvider provideQuestions
     */
    public function testQuestionnaireServiceStore(QuestionnaireRequest $questionnaireRequest, int $expectOpen, int $expectNumerical, bool $expectedIsValid): void
    {
        $databaseService = $this->createMock(DatabaseServiceInterface::class);

        if (!$expectedIsValid) {
            $this->expectException(InvalidRequestException::class);
        } else {
            // mock transaction wrapper fn
            $databaseService->expects($this->once())->method('transactional')->willReturnCallback(function (\Closure $closure) {
                return call_user_func($closure);
            });

            $databaseService->expects($this->any())
                ->method('findBy')
                ->willReturnCallback(function ($class) use ($questionnaireRequest) {
                    if (AnswerOpen::class === $class || AnswerNumerical::class === $class) {
                        // search for duplicates
                        return [];
                    } elseif (Question::class == $class) {
                        // search for questions cll
                        $result = [];

                        /** @var AnswerInterface[] $allAnswers */
                        $allAnswers = array_merge($questionnaireRequest->getOpenAnswers()->getValues(), $questionnaireRequest->getNumericalAnswers()->getValues());

                        foreach ($allAnswers as $answer) {
                            $type = ($answer instanceof AnswerOpen) ? Question::TYPE_NAME_OPEN : Question::TYPE_NAME_NUMERICAL;
                            $result[] = (new Question())->setId($answer->getQuestionId())->setType($type);
                        }

                        return $result;
                    } else {
                        return null;
                    }
                });
        }

        $questionnaireService = $this->getQuestionAnswerService($databaseService, $expectOpen, $expectNumerical);

        $result = $questionnaireService->store($questionnaireRequest, true);

        $this->assertCount(4, $result);
    }

    public function provideQuestions(): \Generator
    {
        yield [
            (new QuestionnaireRequest())
                ->addAnswer((new AnswerNumerical())->setQuestion($this->getQuestion(1))->setAnswer(1)->setUserId(1))
                ->addAnswer((new AnswerNumerical())->setQuestion($this->getQuestion(2))->setAnswer(2)->setUserId(1))
                ->addAnswer((new AnswerOpen())->setQuestion($this->getQuestion(3))->setAnswer('Text answer 1')->setUserId(1))
                ->addAnswer((new AnswerOpen())->setQuestion($this->getQuestion(4))->setAnswer('Text answer 2')->setUserId(1)),
            2,
            2,
            true,
        ];

        // invalid count
        yield [
            (new QuestionnaireRequest())
                ->addAnswer((new AnswerNumerical())->setQuestion($this->getQuestion(1))->setAnswer(1)->setUserId(2))
                ->addAnswer((new AnswerOpen())->setQuestion($this->getQuestion(3))->setAnswer('Text answer 1')->setUserId(2))
                ->addAnswer((new AnswerOpen())->setQuestion($this->getQuestion(4))->setAnswer('Text answer 2')->setUserId(2)),
            2,
            2,
            false,
        ];
    }

    private function getQuestionAnswerService(DatabaseServiceInterface $databaseService, $maxCountOpen, $maxCountNumeral): QuestionAnswerService
    {
        return new QuestionAnswerService($databaseService, $maxCountOpen, $maxCountNumeral, 0, 6);
    }
}
