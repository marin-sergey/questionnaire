<?php

namespace Tests\Feature;

use App\Entities\Questionnaire\Question;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\Console\Output\ConsoleOutput;
use Tests\TestCase;

/**
 * Special test to populate DB with 100k records using API
 *
 * @ignore
 */
class ApiPopulationTest extends TestCase
{
    use WithFaker;

    public function testQuestionnairePopulating()
    {
        $this->markTestSkipped('This test for development purposes only.');

        $questionResponse = $this->get('/api/question');
        $questionList = $questionResponse->json();
        $questionByType = [];
        foreach ($questionList as $_q) {
            if (!array_key_exists($_q['type'], $questionByType)) {
                $questionByType[$_q['type']] = [];
            }
            $questionByType[$_q['type']][] = $_q['id'];
        }

        $maxRequests = 100000 / 10; // 100k by 10 answers in the bucket

        $out = new ConsoleOutput();

        for ($userId=0; $userId <= $maxRequests; $userId++) {
            shuffle($questionByType[Question::TYPE_NAME_OPEN]);
            shuffle($questionByType[Question::TYPE_NAME_NUMERICAL]);

            $openQID = array_slice($questionByType[Question::TYPE_NAME_OPEN], 0, 1);
            $numericQID = array_slice($questionByType[Question::TYPE_NAME_NUMERICAL], 0, 9);

            $answersOpen = array_map(function (int $qid) {
                return ['questionId' => $qid, 'answer' => $this->faker->text()];
            }, $openQID);

            $answersNumerical = array_map(function (int $qid) {
                return ['questionId' => $qid, 'answer' => $this->faker->numberBetween(0, 6)];
            }, $numericQID);

            $answers = array_merge($answersOpen, $answersNumerical);

            $jsonData = json_encode([
                'userId' => $userId,
                'answers' => $answers
            ]);

            /**
             * This is not true http request. It will build new kernel and execute request handler.
             * There may be some side effects with any kind of data buffering or caching services that implements singleton pattern.
             */
            $this->call('POST', '/api/questionnaire', [], [], [], ['Content-Type' => 'application/json; charset=UTF8'], $jsonData);

            $out->writeln($userId);
        }
    }
}
