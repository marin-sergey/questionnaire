<?php

namespace App\Providers;

use App\Service\Database\DatabaseServiceInterface;
use App\Service\Database\DoctrineService;
use App\Service\Domain\Questionnaire\ApiSeederService;
use App\Service\Domain\Questionnaire\QuestionAnswerService;
use App\Service\Domain\Questionnaire\QuestionnaireRequestBuilder;
use Faker\Generator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DatabaseServiceInterface::class, DoctrineService::class);
        $this->app->make(QuestionnaireRequestBuilder::class);

        $this->app->scoped(QuestionAnswerService::class, function ($app) {
            $config = Config::get('questionnaire');

            return new QuestionAnswerService(
                $app->make(DatabaseServiceInterface::class),
                $config['maxCountOpenQuestions'],
                $config['maxCountNumeralQuestions'],
                $config['answerNumericalMin'],
                $config['answerNumericalMax'],
            );
        });

        $this->app->scoped(Serializer::class, function ($app) {
            $normalizers = [new ObjectNormalizer()];
            return new Serializer(
                $normalizers
            );
        });

        $this->app->bind(SerializerInterface::class, Serializer::class);
        $this->app->bind(NormalizerInterface::class, Serializer::class);

        $this->app->scoped(ValidatorInterface::class, function ($app) {
            return Validation::createValidatorBuilder()
                ->enableAnnotationMapping(true)
                ->getValidator();
        });

        $this->app->scoped(ApiSeederService::class, function ($app) {
            $url = config()->get('questionnaire.populate_api');

            return new ApiSeederService(
                $app->make(DatabaseServiceInterface::class),
                $app->make(Generator::class),
                $url
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
