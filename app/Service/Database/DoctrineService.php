<?php

declare(strict_types=1);

namespace App\Service\Database;

use Doctrine\ORM\EntityManagerInterface;

class DoctrineService implements DatabaseServiceInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function transactional(\Closure $closure)
    {
        return $this->entityManager->transactional($closure);
    }

    public function store(object $object)
    {
        $this->entityManager->persist($object);
    }

    public function findBy(string $class, array $criteria): array
    {
        return $this->entityManager->getRepository($class)->findBy($criteria);
    }
}
