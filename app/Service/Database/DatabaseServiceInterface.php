<?php

declare(strict_types=1);

namespace App\Service\Database;

interface DatabaseServiceInterface
{
    public function transactional(\Closure $closure);

    public function store(object $answer);

    public function findBy(string $class, array $criteria);
}
