<?php

declare(strict_types=1);

namespace App\Service\Domain\Questionnaire;

use App\Entities\Questionnaire\AnswerNumerical;
use App\Entities\Questionnaire\AnswerOpen;
use App\Entities\Questionnaire\Question;
use App\Exception\InvalidRequestException;
use App\Request\QuestionnaireRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\Exception\InvalidArgumentException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionnaireRequestBuilder
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     *
     * This method converts json string to QuestionnaireRequest object.
     *
     * By the way, it validates for:
     * - format of input array
     * - question id are uniq
     * - question id exists in DB
     * - type of the answers
     *
     * It did not provide validation of business rules,
     * like the count of questions in the batch or that user answered question.
     *
     * @throws InvalidRequestException
     */
    public function fromJson(string $jsonData): QuestionnaireRequest
    {
        $or = (new OptionsResolver())
            ->setRequired(['userId', 'answers'])
            ->setDefault('answers', function (OptionsResolver $spoolResolver) {
                $spoolResolver->setPrototype(true)
                    ->setRequired(['questionId', 'answer'])
                    ->setAllowedTypes('answer', ['integer', 'string'])
                    ->setAllowedTypes('questionId', 'integer');
            });

        try {
            $data = $or->resolve(json_decode($jsonData, true));
        } catch (InvalidArgumentException $e) {
            // convert OR exception to domain related exception
            throw new InvalidRequestException('Options resolving error: ' . $e->getMessage());
        }

        $questionIdList = array_unique(array_map(function ($item) {
            return $item['questionId'];
        }, $data['answers']));

        if (count($questionIdList) !== count($data['answers'])) {
            // some duplicated question ID found
            throw new InvalidRequestException('Question duplicates found');
        }

        // load questions from DB
        $questions = $this->entityManager->getRepository(Question::class)->findBy(['id' => $questionIdList]);

        if (count($questions) !== count($data['answers'])) {
            // some questions missing in DB
            throw new InvalidRequestException('Missing questions found in Questionnaire batch');
        }

        /**
         * Reindex questions by ID
         * @var Question[]
         */
        $questionsById = array_combine(array_map(function (Question $q) {
            return $q->getId();
        }, $questions), array_values($questions));


        $request = new QuestionnaireRequest();
        foreach ($data['answers'] as $answerArr) {
            /** @var Question $question */
            $question = $questionsById[$answerArr['questionId']];

            switch ($question->getType()) {
                case Question::TYPE_NAME_NUMERICAL:
                    $answer = new AnswerNumerical();
                    break;
                case Question::TYPE_NAME_OPEN:
                    $answer = new AnswerOpen();
                    break;
                default:
                    throw new InvalidRequestException("Unsupported question type found for question ID={$question->getId()}");
            }

            try {
                $answer->setUserId($data['userId'])
                    ->setQuestion($question)
                    ->setAnswer($answerArr['answer']);
            } catch (\TypeError $e) {
                // invalid type answer.
                throw new InvalidRequestException("Wrong answer type for question with ID={$answerArr['questionId']}");
            }

            $request->addAnswer($answer);
        }

        return $request;
    }
}
