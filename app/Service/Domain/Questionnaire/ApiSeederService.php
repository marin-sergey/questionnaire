<?php

declare(strict_types=1);

namespace App\Service\Domain\Questionnaire;

use App\Entities\Questionnaire\Question;
use App\Service\Database\DatabaseServiceInterface;
use Faker\Generator;
use Illuminate\Support\Facades\Http;

class ApiSeederService
{
    private \Closure $onResultCallable;

    public function __construct(private DatabaseServiceInterface $database, private Generator $faker, private string $apiUrl)
    {
    }

    public function onResult(\Closure $onResultCallable): self
    {
        $this->onResultCallable = $onResultCallable;
        return $this;
    }

    public function start(int $count, int $start)
    {
        /** @var Question[] $questionList */
        $questionList = $this->database->findBy(Question::class, []);
        $questionByType = [];
        foreach ($questionList as $_q) {
            $type = $_q->getType();
            if (!array_key_exists($type, $questionByType)) {
                $questionByType[$type] = [];
            }
            $questionByType[$type][] = $_q->getId();
        }

        for ($userId=$start;$userId<($start+$count);$userId++) {
            shuffle($questionByType[Question::TYPE_NAME_OPEN]);
            shuffle($questionByType[Question::TYPE_NAME_NUMERICAL]);

            $openQID = array_slice($questionByType[Question::TYPE_NAME_OPEN], 0, 1);
            $numericQID = array_slice($questionByType[Question::TYPE_NAME_NUMERICAL], 0, 9);

            $answersOpen = array_map(function (int $qid) {
                return ['questionId' => $qid, 'answer' => $this->faker->text()];
            }, $openQID);

            $answersNumerical = array_map(function (int $qid) {
                return ['questionId' => $qid, 'answer' => $this->faker->numberBetween(0, 6)];
            }, $numericQID);

            $answers = array_merge($answersOpen, $answersNumerical);

            $response = Http::withHeaders([
                'Accept' => 'application/json'
            ])->post($this->apiUrl, [
                'userId' => $userId,
                'answers' => $answers
            ]);

            if ($this->onResultCallable) {
                call_user_func($this->onResultCallable, $response, $userId);
            }
        }
    }
}
