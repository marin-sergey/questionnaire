<?php

declare(strict_types=1);

namespace App\Service\Domain\Questionnaire;

use App\Entities\Questionnaire\AnswerNumerical;
use App\Entities\Questionnaire\AnswerOpen;
use App\Entities\Questionnaire\Question;
use App\Exception\InvalidRequestException;
use App\Request\QuestionnaireRequest;
use App\Service\Database\DatabaseServiceInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionAnswerService
{
    public function __construct(
        private DatabaseServiceInterface $databaseService,
        private int $maxCountOpenQuestions,
        private int $maxCountNumeralQuestions,
        private int $answerNumericalMin,
        private int $answerNumericalMax
    ) {
    }

    /**
     * This method validates for business rules:
     * - that object contains required amount of answers
     * - and there is no duplicates of user + question.
     * - numerical answer respects defined bounds
     */
    public function isValid(QuestionnaireRequest $request): bool
    {
        /*
         * Here is very simple validation
         * It better to replace with some framework provided solution
         */
        if ($request->getOpenAnswers()->count() !== $this->maxCountOpenQuestions || $request->getNumericalAnswers()->count() !== $this->maxCountNumeralQuestions) {
            throw new InvalidRequestException('Wrong count of questions in request');
        }

        /** @var AnswerOpen $answer */
        if ($answer = $request->getOpenAnswers()->first()) {
            $criteria = ['userId' => $answer->getUserId(), 'question' => $request->getOpenQuestionIdList()];
            if ($this->databaseService->findBy(AnswerOpen::class, $criteria)) {
                throw new InvalidRequestException('Duplicated open answers found');
            }
        }

        /** @var AnswerNumerical $answer */
        if ($answer = $request->getNumericalAnswers()->first()) {
            $criteria = ['userId' => $answer->getUserId(), 'question' => $request->getNumericalQuestionIdList()];
            if ($this->databaseService->findBy(AnswerNumerical::class, $criteria)) {
                throw new InvalidRequestException('Duplicated numerical answers found');
            }
        }

        foreach ($request->getNumericalAnswers() as $answer) {
            $value = $answer->getAnswer();
            if ($value < $this->answerNumericalMin || $value > $this->answerNumericalMax) {
                throw new InvalidRequestException('Answer value out of bounds');
            }
        }

        return true;
    }

    /**
     * @return Question[]
     * @throws InvalidRequestException
     */
    public function store(QuestionnaireRequest $questionnaireRequest, ?bool $validate): array
    {
        if ($validate) {
            $this->isValid($questionnaireRequest);
        }

        /**
         * Using transaction is not optimal by performance, but it allows to be sure about DB consistency
         *
         * There is a way to optimize performance of insert and update:
         * - rewrite 10 inserts & 10 updates into 2 `batch` queries
         *  INSERT INTO tbl_name (a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);
         *  UPDATE ... SET prop = (CASE id WHEN 1 THEN 'val1' WHEN 2 THEN 'val2' WHEN 3 THEN 'val3' END) WHERE id IN (1,2,3);
         */
        return $this->databaseService->transactional(function () use ($questionnaireRequest) {
            $allAnsweredQuestions = [];
            $allAnswers = array_merge(
                $questionnaireRequest->getOpenAnswers()->getValues(),
                $questionnaireRequest->getNumericalAnswers()->getValues(),
            );

            foreach ($allAnswers as $answer) {
                $question = $answer->getQuestion();
                $this->databaseService->store($answer);
                $question->updateAggregatedValues($answer);
                $this->databaseService->store($question);
                $allAnsweredQuestions[$question->getId()] = $question;
            }

            return $allAnsweredQuestions;
        });
    }
}
