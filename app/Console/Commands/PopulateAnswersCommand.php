<?php

namespace App\Console\Commands;

use App\Service\Domain\Questionnaire\ApiSeederService;
use Illuminate\Console\Command;
use Illuminate\Http\Client\Response;

class PopulateAnswersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:questionnaire:populate {count=1} {start=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate database with Answers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(ApiSeederService $apiSeederService)
    {
        $count = (int)$this->argument('count');
        $start = (int)$this->argument('start');

        $apiSeederService->onResult(function (Response $response, int $userId) {
            $this->line('HTTP Status '.$response->status() .' for user #'. $userId);
            $this->line($response->body());
        })->start($count, $start);

        return 0;
    }
}
