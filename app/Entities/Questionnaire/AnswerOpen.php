<?php

declare(strict_types=1);

namespace App\Entities\Questionnaire;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questionnaire__answer_open", indexes={
 *     @ORM\Index(name="ao_user_question_idx", columns={"user_id", "question_id"})
 * })
 */
class AnswerOpen extends AbstractAnswer implements AnswerInterface
{
    /**
     * @ORM\Column(type="string")
     */
    public string $answer;

    public function setAnswer(string $answer): AnswerInterface
    {
        $this->answer = $answer;

        return $this;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }
}
