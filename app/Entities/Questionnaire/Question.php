<?php

declare(strict_types=1);

namespace App\Entities\Questionnaire;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="questionnaire__question")
 */
class Question
{
    public const TYPE_NAME_OPEN = 'open';
    public const TYPE_NAME_NUMERICAL = 'numerical';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $type;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private string $questionText;

    /**
     * @ORM\Column(type="integer")
     */
    private int $total; // count of answers

    /**
     * @ORM\Column(type="integer")
     */
    private int $sum; // sum of values or sum of word count

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private float $average; // average value of values or word count

    public function __construct()
    {
        $this->total = 0;
        $this->sum = 0;
        $this->average = 0;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function updateAggregatedValues(AnswerInterface $answer)
    {
        ++$this->total;

        if ($answer instanceof AnswerOpen) {
            $this->sum += str_word_count($answer->getAnswer());
        } elseif ($answer instanceof AnswerNumerical) {
            $this->sum += $answer->getAnswer();
        }

        $this->average = round($this->sum / $this->total, 2);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, [self::TYPE_NAME_NUMERICAL, self::TYPE_NAME_OPEN])) {
            throw new \InvalidArgumentException('Question type must be `numerical` or `open`');
        }

        $this->type = $type;

        return $this;
    }

    public function getAverage(): float
    {
        return $this->average;
    }

    public function getQuestionText(): string
    {
        return $this->questionText;
    }

    public function setQuestionText(string $questionText): self
    {
        $this->questionText = $questionText;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getSum(): int
    {
        return $this->sum;
    }
}
