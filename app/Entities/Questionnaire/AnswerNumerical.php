<?php

declare(strict_types=1);

namespace App\Entities\Questionnaire;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questionnaire__answer_numerical", indexes={
 *     @ORM\Index(name="an_user_question_idx", columns={"user_id", "question_id"})
 * })
 */
class AnswerNumerical extends AbstractAnswer implements AnswerInterface
{
    /**
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private int $answer;

    public function setAnswer(int $answer): AnswerInterface
    {
        $this->answer = $answer;

        return $this;
    }

    public function getAnswer(): int
    {
        return $this->answer;
    }
}
