<?php

declare(strict_types=1);

namespace App\Entities\Questionnaire;

interface AnswerInterface
{
    public function getQuestionId(): int;

    public function getQuestion(): Question;

    public function setQuestion(Question $question): self;

    public function getUserId(): int;

    public function setUserId(int $userId): self;
}
