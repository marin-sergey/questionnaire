<?php

namespace App\Http\Controllers;

use App\Entities\Questionnaire\Question;
use App\Exception\InvalidRequestException;
use App\Request\QuestionnaireRequest;
use App\Service\Domain\Questionnaire\QuestionAnswerService;
use App\Service\Domain\Questionnaire\QuestionnaireRequestBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class QuestionController extends BaseController
{
    private const DEFAULT_LIMIT = 30;

    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;


    public function __construct(private EntityManagerInterface $entityManager, private NormalizerInterface $normalizer)
    {
    }

    /**
     * List of questions.
     *
     * Use query parameter page, to paginate result
     *
     */
    public function index(Request $request)
    {
        $repository = $this->entityManager->getRepository(Question::class);
        $page = $request->get('page', 1);

        $questions = $repository->findBy(
            [],
            ['id' => 'DESC'],
            self::DEFAULT_LIMIT,
            self::DEFAULT_LIMIT * ($page>0?$page-1:0)
        );

        $questionsList = $this->normalizer->normalize($questions);

        return response()
            ->json($questionsList, 200)
            ->header('Content-Type', 'application/json');
    }
}
