<?php

namespace App\Http\Controllers;

use App\Exception\InvalidRequestException;
use App\Service\Domain\Questionnaire\QuestionAnswerService;
use App\Service\Domain\Questionnaire\QuestionnaireRequestBuilder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class QuestionnaireController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;


    public function __construct(
        private QuestionAnswerService $questionAnswerService,
        private QuestionnaireRequestBuilder $requestBuilder,
        private ValidatorInterface $validator
    ) {
    }

    public function create(Request $request)
    {

        /**
         * First stage: create domain's request object
         *
         */
        try {
            $qRequest = $this->requestBuilder->fromJson($request->getContent());

            /***
             * Validation may be provided using Symfony component and entity annotations.
             * But it requires additional time to research of annotation caching and related services implementation.
             */

            /*$validationResult = $this->validator->validate($qRequest);

            if ($validationResult->count() > 0) {
                $errorsString = (string) $validationResult;
                return response()
                    ->json(['error' => $errorsString], Response::HTTP_UNPROCESSABLE_ENTITY)
                    ->header('Content-Type', 'application/json');
            }*/

            /**
             * Putting questions to storage
             */
            $this->questionAnswerService->store($qRequest, true);
        } catch (InvalidRequestException $e) {
            return response()
                ->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST)
                ->header('Content-Type', 'application/json');
        }

        $result = [
            'an_avg' =>  $qRequest->getAvgNumericalAnswer(), // sum of answer values devided by count of answers in request
            'an_sum' => $qRequest->getSumOfNumericalAnswers(), // sum of answer values in request
            'an_answers_per_question_total' => $qRequest->getNumericalAnswersPerQuestionMap(), // count of answers per question for the all-time
            'ao_answers_per_question' => $qRequest->getNumericalAnswersPerQuestionMap(), // the same, but for open type
            'ao_word_count_total' => $qRequest->getOpenAnswersWordCountTotal(), // count of words per question for the all-time
        ];

        return response()
            ->json($result, 200)
            ->header('Content-Type', 'application/json');
    }
}
