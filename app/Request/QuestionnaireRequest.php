<?php

declare(strict_types=1);

namespace App\Request;

use App\Entities\Questionnaire\AnswerInterface;
use App\Entities\Questionnaire\AnswerNumerical;
use App\Entities\Questionnaire\AnswerOpen;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class QuestionnaireRequest
{
    /**
     * @var ArrayCollection|AnswerOpen[]
     *
     * @Assert\Valid(traverse="true")
     */
    private ArrayCollection $openAnswers;

    /**
     * @var ArrayCollection|AnswerNumerical
     *
     * @Assert\Valid(traverse="true")
     */
    private ArrayCollection $numericalAnswers;

    public function __construct()
    {
        $this->openAnswers = new ArrayCollection();
        $this->numericalAnswers = new ArrayCollection();
    }

    /**
     * @return AnswerOpen[]|ArrayCollection
     */
    public function getOpenAnswers(): ArrayCollection
    {
        return $this->openAnswers;
    }

    /**
     * @return AnswerNumerical[]|ArrayCollection
     */
    public function getNumericalAnswers(): ArrayCollection
    {
        return $this->numericalAnswers;
    }

    public function addAnswer(AnswerInterface $answer): self
    {
        if ($answer instanceof AnswerOpen) {
            $this->openAnswers->set($answer->getQuestionId(), $answer);
        } elseif ($answer instanceof AnswerNumerical) {
            $this->numericalAnswers->set($answer->getQuestionId(), $answer);
        }

        return $this;
    }

    /**
     * @return int[]
     */
    public function getOpenQuestionIdList(): array
    {
        return array_map(function (AnswerOpen $answer) {
            return $answer->getQuestionId();
        }, $this->getOpenAnswers()->getValues());
    }

    /**
     * @return int[]
     */
    public function getNumericalQuestionIdList(): array
    {
        return array_map(function (AnswerNumerical $answer) {
            return $answer->getQuestionId();
        }, $this->getNumericalAnswers()->getValues());
    }

    /**
     * @return int[]
     */
    public function getAllQuestionIdList(): array
    {
        return array_merge($this->getOpenQuestionIdList(), $this->getNumericalQuestionIdList());
    }

    public function getAvgNumericalAnswer():float
    {
        $sum = $this->getSumOfNumericalAnswers();
        $count = $this->getNumericalAnswers()->count();

        return round($sum/$count);
    }

    public function getSumOfNumericalAnswers():int
    {
        return array_sum($this->getNumericalAnswers()->map(fn (AnswerNumerical $a) => $a->getAnswer())->getValues());
    }

    /**
     * @return int[]
     */
    public function getNumericalAnswersPerQuestionMap(): array
    {
        $questionToTotalMap = [];
        foreach ($this->getNumericalAnswers() as $answer) {
            $questionToTotalMap[$answer->getQuestionId()] = $answer->getQuestion()->getTotal();
        }

        return $questionToTotalMap;
    }

    /**
     * @return int[]
     */
    public function getOpenAnswersWordCountTotal(): array
    {
        $questionToTotalMap = [];
        foreach ($this->getOpenAnswers() as $answer) {
            $questionToTotalMap[$answer->getQuestionId()] = $answer->getQuestion()->getSum();
        }

        return $questionToTotalMap;
    }
}
