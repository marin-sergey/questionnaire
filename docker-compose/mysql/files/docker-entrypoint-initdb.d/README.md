Place SQL dump files to initialize mysql on first run

To force reinitialization:
1. stop docker-compose containers
2. remove all contents of `./docker-compose/mysql/files/var/lib/mysql` (sudo required)
3. start docker-compose containers