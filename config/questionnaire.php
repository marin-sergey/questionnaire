<?php

return [
    'maxCountOpenQuestions' => 1,
    'maxCountNumeralQuestions' => 9,
    'answerNumericalMin' => 0,
    'answerNumericalMax' => 6,

    'populate_api' => 'http://localhost:'.env('PHP_DEV_SERVER_PORT', '80').'/api/questionnaire',
];
